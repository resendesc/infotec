/**
 * @file
 * Image-Recognition CAPTCHA JavaScript
 */

(function ($) {

  "use strict";

  /**
   * Allows image selection by clicking image.
   *
   * This works by hiding the checkboxes used to select images in browsers
   * without JavaScript, but still toggling them by script when the image is
   * clicked.
   */
  Drupal.behaviors.irCaptchaImageSelectionByClick = {
    attach: function (context, settings) {
      $('.captcha', context).once('imageSelectionByClick', function () {
        var $captcha = $(this);

        // Hide checkboxes.
        $('.ir-captcha-response', $captcha).hide();

		// Handle image click.
        $('.ir-captcha-image', $captcha).click(function () {
		  var $challenge_item = $(this).parent();

          // Toggle corresponding checkbox.
          var $checkbox = $challenge_item.find('.ir-captcha-response .form-checkbox');
          $checkbox.attr('checked', !$checkbox.attr('checked'));

          // Toggle selection indicator.
          $challenge_item.toggleClass('ir-captcha-image-selected');
        });
      });
    }
  };

})(jQuery);
