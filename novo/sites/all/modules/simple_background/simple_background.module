<?php
/**
 * @file
 * Module file for simple_background.
 */

/**
 * Implements hook_menu().
 */
function simple_background_menu() {
  $items['admin/config/user-interface/simple_background'] = array(
    'title' => 'Simple background',
    'description' => 'Set your background image.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('simple_background_settings_form'),
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('administer background'),
  );

  return $items;
}

/**
 * Implements hook_permission.
 */
function simple_background_permission() {
  return array(
    'administer background' => array(
      'title' => t('Administer background image'),
      'description' => t('Access background image configuration.'),
    ),
  );
}

/**
 * Settings form.
 */
function simple_background_settings_form($form, &$form_state) {
  $settings = unserialize(variable_get('simple_background'));

  // Show on admin pages.
  $form['admin'] = array(
    '#title' => t('Show on admin pages'),
    '#description' => t('Background image will be shown on admin pages.'),
    '#type' => 'checkbox',
    '#default_value' => !empty($settings['admin']) ? $settings['admin'] : '',
  );

  // Fieldset for CSS settings.
  $form['background'] = array(
    '#title' => t('CSS settings'),
    '#type' => 'fieldset',
  );

  // CSS selector.
  $form['background']['selector'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS selector'),
    '#description' => t('Enter a CSS selector for the background image to attach to.'),
    '#default_value' => !empty($settings['selector']) ? $settings['selector'] : 'body',
  );

  // Background image.
  $form['background']['image'] = array(
    '#type' => 'managed_file',
    '#title' => t('Image'),
    '#description' => 'Upload an image to use as background image.',
    '#progress_indicator' => 'bar',
    '#upload_location' => 'public://simple_background',
    '#upload_validators' => array(
      'file_validate_extensions' => array('jpg jpeg png gif'),
    ),
    '#default_value' => !empty($settings['image']) ? $settings['image'] : '',
  );

  // Background repeat.
  $form['background']['repeat'] = array(
    '#type' => 'select',
    '#title' => t('Repeat'),
    '#description' => t('Select how the background image will repeat itself.'),
    '#options' => array(
      'no-repeat' => t('No repeat'),
      'repeat' => t('Repeat'),
      'repeat-x' => t('Horizontal repeat'),
      'repeat-y' => t('Vertical repeat'),
    ),
    '#default_value' => !empty($settings['repeat']) ? $settings['repeat'] : '',
  );

  // Background size.
  $form['background']['size'] = array(
    '#type' => 'select',
    '#title' => t('Background size'),
    '#description' => t('Select how the background image will fill out.'),
    '#options' => array(
      'auto' => t('Original size'),
      'contain' => t('Stretch image to fit'),
      'cover' => t('Fill whole page'),
    ),
    '#default_value' => !empty($settings['size']) ? $settings['size'] : '',
  );

  // Background attachment.
  $form['background']['attachment'] = array(
    '#type' => 'select',
    '#title' => t('Background attachment'),
    '#description' => t('Select how the background image will fill out.'),
    '#options' => array(
      'scroll' => t('Background image scrolls with the page'),
      'fixed' => t('Background image is fixed on screen'),
    ),
    '#default_value' => !empty($settings['attachment']) ? $settings['attachment'] : '',
  );

  // Background centering.
  $form['background']['center'] = array(
    '#type' => 'checkbox',
    '#title' => t('Center background'),
    '#description' => t('Center the background image horizontally.'),
    '#default_value' => !empty($settings['center']) ? $settings['center'] : '',
  );

  // Submit.
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Submit handler.
 */
function simple_background_settings_form_submit(&$form, &$form_state) {
  // Get simple background settings.
  $settings = variable_get('simple_background', NULL);

  // If the user tries to upload a file.
  if (!empty($form_state['values']['image'])) {
    // Get rid of old image if exists.
    if (!empty($settings)) {
      $settings = unserialize($settings);
      // Check for file and if it's the same file.
      if (!empty($settings['image']) AND ($settings['image'] != $form_state['values']['image'])) {
        // Load file.
        $file = file_load($settings['image']);
        // Delete file.
        if (!empty($file)) {
          file_delete($file, TRUE);
        }
      }
      // Unset the file variable.
      unset($file);
    }

    // Load file.
    $file = file_load($form_state['values']['image']);
    // Make file permanent.
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    // Register usage.
    file_usage_add($file, 'simple_background', 'simple_background', $form_state['values']['image']);

    // Set image settings.
    variable_set('simple_background', serialize($form_state['values']));
    // Congratulate the user on a successful upload.
    drupal_set_message(t('Background image saved.'));
  }

  // Else if settings exists and no file is uploaded.
  elseif (!empty($settings)) {
    $settings = unserialize($settings);
    // Check for file.
    if (!empty($settings['image'])) {
      // Load file.
      $file = file_load($settings['image']);
      // Delete file.
      if (!empty($file)) {
        file_delete($file, TRUE);

        // Provide feedback to the user of image deletion.
        drupal_set_message(t('Background image deleted.'), 'warning');
      }
    }

    // Provide success message of settings update.
    drupal_set_message(t('Updated Simple Background settings.'));
  }

  // If there is no existing settings and image trying to be uploaded.
  else {
    // Save the settings despite missing an image file, so we can pre-populate
    // the Simple Background settings form with the previous data.
    variable_set('simple_background', serialize($form_state['values']));
    // Provide the user with feedback about a missing image.
    drupal_set_message(t('Select an image to save.'), 'error');
  }
}

/**
 * Implements template_preprocess_html().
 */
function simple_background_preprocess_html(&$vars) {
  // Get settings.
  $settings = unserialize(variable_get('simple_background'));

  // No dice on admin pages if setting.
  if (!$settings['admin'] && arg(0) == 'admin') {
    return;
  }
  // Get file id.
  $fid = !empty($settings['image']) ? $settings['image'] : FALSE;
  // Load file.
  $bg_image = is_numeric($fid) ? file_load($fid) : FALSE;

  if ($bg_image) {
    // Map settings into variables.
    $url = file_create_url($bg_image->uri);

    $selector = $settings['selector'];
    $repeat = $settings['repeat'];
    $size = $settings['size'];
    $attachment = $settings['attachment'];
    $center = $settings['center'];
    $center_css = $center ? "background-position: top center !important;" : '';

    // Build CSS.
    $css = <<<EOT
$selector {
  background-image: url($url) !important;
  background-repeat: $repeat !important;
  background-size: $size !important;
  background-attachment: $attachment !important;
  $center_css
}
EOT;

    // Add CSS.
    drupal_add_css($css, array('type' => 'inline'));
  }
}
